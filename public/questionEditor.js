let questionEditor = {
    currentQuestions: [],
    currentQuestion: {},
    theQuestionsDiv: null,
    beginEdit: async function() {
        this.theQuestionsDiv = $("#editQuestions");
        this.currentQuestions = await getAllQuestions();
        console.log("this.currentQuestions:", this.currentQuestions);
        this.renderQuestionSelector();
        this.theQuestionsDiv.show();
    },
    beginAdd: function() {
        this.theQuestionsDiv = $("#addQuestion");
        this.currentQuestions = {};
        console.log("this.currentQuestions:", this.currentQuestions);
        this.renderAddQuestion();
        this.theQuestionsDiv.show();
    },
    renderQuestionSelector: function() {
        let selector = $("<select id='questionSelector'></select>");
        selector.append("<option value='NONE'>Step 1 pick a question</option>");
        for(let i =0; i<this.currentQuestions.length; i++) {
            selector.append("<option value='"+this.currentQuestions[i].id+"'>"+this.currentQuestions[i].name+"</option>");
        }
        this.theQuestionsDiv.append(selector);
        selector.off("change").on("change", function(){
            questionEditor.renderSelectedQuestion($(this).val());
        })
    },
    renderSelectedQuestion: function(question_id) {
        $("#q_question_div").remove();
        $("#q_answers_div").remove();
        if(question_id == "NONE") return;
        this.currentQuestion = this.getQuestionById(question_id);
        let question_question = $("<div id='q_question_div'><h2>Step 2 update Question text and or Answers</h2>Question:<input type='text' id='q_question' value='"+this.currentQuestion.question+"'></input></div>");
        let question_answers = $("<div id='q_answers_div'>Answers(use 3 pipes:'|||' between each answer):<input type='text' id='q_answers' value='"+this.currentQuestion.answers.join("|||")+"'></input></div>");
        this.theQuestionsDiv.append(question_question);
        this.theQuestionsDiv.append(question_answers);
        $("#q_answers").off("change").on("change", this.renderQuestionAnswerSelector);
        this.renderQuestionAnswerSelector();
    },
    renderAddQuestion: function(question_id) {
        $("#q_question_div").remove();
        $("#q_answers_div").remove();
        
        let question_name = $("<div id='q_name_div'><h2>Step 1 give your question a name(use only letters and spaces</h2>Name:<input type='text' id='q_name' value=''></input></div>");
        let question_question = $("<div id='q_question_div'><h2>Step 2 create Question text and Answers</h2>Question:<input type='text' id='q_question' value=''></input></div>");
        let question_answers = $("<div id='q_answers_div'>Answers(use 3 pipes:'|||' between each answer):<input type='text' id='q_answers' value=''></input></div>");
        this.theQuestionsDiv.append(question_name);
        this.theQuestionsDiv.append(question_question);
        this.theQuestionsDiv.append(question_answers);
        $("#q_answers").off("change").on("change", this.renderQuestionAnswerSelector);
        this.renderQuestionAnswerSelector();
    },
    renderQuestionAnswerSelector: function() {
        $("#q_answer_selector").remove();
        $("#q_save_div").remove();
        let answers = $("#q_answers").val().split("|||");
        console.log("renderQuestionAnswerSelector:", answers);
        let selector = $("<select id='q_answer_selector'></select>");
        selector.append("<option value='NONE'>Step 3 pick the answer</option>");
        for(let i =0; i<answers.length; i++) {
            selector.append("<option value='"+answers[i]+"'>"+answers[i]+"</option>");
        }
        questionEditor.theQuestionsDiv.append(selector);

        selector.off("change").on("change", function(){
            if($(this).val() == "NONE") {
                $("#q_save_div").remove();
            } else {
                questionEditor.theQuestionsDiv.append("<div id='q_save_div'><button id='q_save'>Save Changes to Database</button></div>");
                $("#q_save").off("click").on("click", questionEditor.submitEdits );
            }
        })
    },
    submitEdits: function() {
        if(typeof(questionEditor.currentQuestion.name) == "undefined") {
            let name = $("#q_name").val();
            if(name == "") {
                alert("question must have a name");
                return;
            }
            questionEditor.currentQuestion.name = name;
        }
        questionEditor.currentQuestion.question = $("#q_question").val();
        questionEditor.currentQuestion.answers = $("#q_answers").val().split("|||");
        questionEditor.currentQuestion.correctAnswer = $("#q_answer_selector").val();
        questionToDataBase(questionEditor.currentQuestion);
        $("#q_name").remove();
        $("#q_answer_selector").remove();
        $("#q_save_div").remove();
        $("#q_question_div").remove();
        $("#q_answers_div").remove();
        alert("Question updated");
        questionEditor.theQuestionsDiv.hide();
    },
    getQuestionById: function(question_id) {
        for(let i =0; i<this.currentQuestions.length; i++) {
            if(this.currentQuestions[i].id == question_id) {
                return this.currentQuestions[i];
                break;
            }
        }
    }

}