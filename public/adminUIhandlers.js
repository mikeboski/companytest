
let questions = null;
//on ready
$(function(){
    $("#makeDefaultQuestionsInDbBtn").off("click").on("click", makeDefaultQuestionsInDb);
    $("#getAllQuestionsBtn").off("click").on("click", function() {
        questions = getAllQuestions();
    });
    $("#clearQuestionsInDbBtn").off("click").on("click", function() {
        if(confirm("Are you sure you want to delete all questions?")) {
            deleteAllQuestions();
            alert("All questions deleted.");
        } else {
            alert("Delete all questions canceled.");         
        }
    });

    $("#editQuestionsBtn").off("click").on("click", function() {
        questionEditor.beginEdit();
    });
    $("#addQuestionBtn").off("click").on("click", function() {
        questionEditor.beginAdd();
    });
    
    
    
});