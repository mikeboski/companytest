"use strict";

let quiz = {
    correctAnswers: 0,
    currentQuestionIndex: 0,
    totalQuestions: 0,
    currentQuestions: [],
    currentQuestion: {},
    beginQuiz: async function() {
        this.currentQuestions = await getAllQuestions();
        this.totalQuestions = this.currentQuestions.length;
        this.correctAnswers= 0;
        this.currentQuestionIndex= 0,
        this.showQuestion(this.currentQuestionIndex);
    },
    showQuestion: function(index) {
        this.currentQuestion = this.currentQuestions[index];
        const question_question = $("<div class='question_question'>"+this.currentQuestion.question+"</div>");
        $("body").append(question_question);
        const answers = this.currentQuestion.answers;
        for(let i =0; i<answers.length; i++) {
            $("body").append("<button class='answers'>"+answers[i]+"</button>");
        }
        $(".answers").off("click").on("click", function(){
            const answer_str =  $(this).text();
            quiz.answerQuestion(answer_str);
        });
    },
    answerQuestion: function(answer_str) {
        if(checkAnswer(this.currentQuestion, answer_str)) {
            console.log("yeah got that one right");
            this.correctAnswers++;
        } else {
            console.log("darn it. got that one wrong");
        }
        this.nextQuestion();
    },
    nextQuestion: function() {
        this.clearLastRenderedQuestion();
        this.currentQuestionIndex++;
        if(this.currentQuestionIndex >= this.totalQuestions) { 
            console.log("Quiz is over let show how they did");
            this.showResults();
        } else {
            this.showQuestion(quiz.currentQuestionIndex);
        }
    },
    clearLastRenderedQuestion: function() {
        $(".question_question").remove();
        $(".answers").remove();
    },
    showResults: function() {
        const results_str = "You got " +this.correctAnswers+" answers correct out of "+this.totalQuestions;
        $("body").append(results_str);
    }

}
//ready set go
$(
    function() {
        quiz.beginQuiz();
    }
)