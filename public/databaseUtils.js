"use strict";
const auth = window.firebase.auth();

auth.onAuthStateChanged(currentUser => console.log(currentUser));
auth.signInAnonymously();

function questionToDataBase(_obj) {
    //get hash of answer so user can not see it in JSON network traffic
    const salt = "HappyHippo";
    const hash = tool(salt + "." + _obj.answers.join(".") + "." + _obj.correctAnswer);
    const db = firebase.firestore();
    const docname = _obj.name.toLowerCase().split(" ").join("");
    const docRef = db.collection('questions').doc(docname);
    _obj.guid = hash;
    delete _obj.correctAnswer;
    console.log("questionToDataBase:", _obj);
    docRef.set(_obj);
}

function deleteAllQuestions() {
    var db = firebase.firestore();
    db.collection("questions").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            db.collection("questions").doc(doc.id).delete();
        });
    });
}
async function getAllQuestions() {
    var db = firebase.firestore();
    var resultArray =[];
    await db.collection("questions").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
            var question = doc.data();
            question.id = doc.id;
            resultArray.push(question);
        });
    });
    return resultArray;
}