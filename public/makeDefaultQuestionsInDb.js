"use strict";

function makeDefaultQuestionsInDb() {
    questionToDataBase({
        question: 'Are you a fish?',
        answers: ['What is a fish?', 'Yes', 'No', 'Maybe'],
        name: 'Fish Question',
        correctAnswer: 'No'
    });

    questionToDataBase({
        question: 'Is time an illusion?',
        answers: ['No', 'Yes, lunch time doubly so.', 'Who is Douglas Adams?', 'Mostly Harmless'],
        name: 'Time Question',
        correctAnswer: 'Yes, lunch time doubly so.'
    });

    questionToDataBase({
        question: 'What is the interviewee\'s name',
        answers: ['Bob', 'Mike', 'Douglas Adams', 'OJ'],
        name: 'Name Question',
        correctAnswer: 'Mike'
    });
}
