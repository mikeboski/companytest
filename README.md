#Simple Quiz app.

This simple quiz app use Firebase Firestore for Database and Firebase hosting to server the files.

##Client Requirements:
- 3 questions with 4 multiple choice answers per question.
- The survey should be dynamic i.e. easily update the number of questions and answers
- The data needed to create the survey, questions, answers etc. should be consumed from an API endpoint. That can be a nodeJS Server returning a mock JSON.
- Answering one question automatically progresses the user to the next one.
- Keep track of the correct answers and at the last frame display how many correct answers the user got

##Things of interest:
- The Answers are saved as a hash in the GUID field to try and prevent anyone from looking at the JSON file coming or actually opening the console and just logging out the answer.
* Since answers are only stored as a hash you must select the answer each time you update a question.
* tool.min.js is actually md5.min.js just renamed the function to through off anyone trying to reverse engenieer that HASH
* checkAnswer.js file is obfuscated to help prevent someone from reverse engenieering and figuring out the salt of the HASH


## Demo links and information
### Working version of the quiz can be viewed at https://quartz-quiz.web.app/

#### The correct answers:
```javascript
{
        question: 'Are you a fish?',
        answers: ['What is a fish?', 'Yes', 'No', 'Maybe'],
        name: 'Fish Question',
        correctAnswer: 'No'
},
{
        question: 'Is time an illusion?',
        answers: ['No', 'Yes, lunch time doubly so.', 'Who is Douglas Adams?', 'Mostly Harmless'],
        name: 'Time Question',
        correctAnswer: 'Yes, lunch time doubly so.'
},
{
    question: 'What is the interviewee\'s name',
    answers: ['Bob', 'Mike', 'Douglas Adams', 'OJ'],
    name: 'Name Question',
    correctAnswer: 'Mike'
}
```

### Admin panel is avialable at https://quartz-quiz.web.app/admin.html
With it you can 
* delete all questions
* edit a current question
* add a new quesiton
* reset the default questions

# Deployment
NOTE: 
- if you want to install this on your own instance of Firebase you will have to setup your new project: https://firebase.google.com/docs/storage/web/start
- You also will need to setup Firestore https://firebase.google.com/docs/firestore/quickstart

Update index.html and admin.html code here:
```javascript
 var firebaseConfig = {
            apiKey: "YOUR KEY",
            authDomain: "YOUR DOMAIN",
            databaseURL: "YOUR DB URL",
            projectId: "YOURID",
            storageBucket: "",
            messagingSenderId: "YOUR_ID",
            appId: "YOUR_ID"
        };
```


using the CLI for firebase https://firebase.google.com/docs/cli

from the root of this git repo:
```sh
firebase login

firebase init

firebase deploy
```


